# AppSec Portal

# AppSec Portal

![Whitespots logo](/docs/images/appsec_portal_logo.jpeg "AppSec Portal")

**AppSec Portal** is a defect managment system that helps your business to be secured
against cyberattacks.

<details><summary>Expand to see more screenshots</summary>
![Dashboard](/docs/images/dashboard.png "Dashboard")
![All Findings view](/docs/images/all_findings_view.gif "All Findings view")
![Products Tab](/docs/images/products_tab.png "Products tab")
![Business criticality settings](/docs/images/business_criticality_settings.gif "Business criticality settings")
</details>

To obtain a license for AppSec Portal, register for an account on [store.whitespots.io](https://store.whitespots.io/), sign in, and request a **free license** or **contact sales** for paid AppSec Portal features.

**Free version**
- [Easy triage](https://docs.whitespots.io/appsec-portal/features/modern-responsive-interface): simplify and prioritize issues that require attention. 🚨
- Application Security processes establishment guide 📝
- [SLA (Service Level Agreement)](https://docs.whitespots.io/appsec-portal/features/security-metrics/sla), [WRT (Weighted Risk Trend)](https://docs.whitespots.io/appsec-portal/features/security-metrics/wrt-weighted-risk-trend) settings (look for more metrics [here](https://owasp.org/www-pdf-archive/Magic_Numbers_-_5_KPIs_for_Measuring_WebAppSec_Program_Success_v3.2.pdf)). 📊
- [Risk assessment survey](https://docs.whitespots.io/appsec-portal/features/risk-assessment-overview): Evaluate and identify potential security risks. 📈
- [Basic deduplication](https://docs.whitespots.io/appsec-portal/features/deduplicator): based on same file paths, same line numbers, same titles and same scanners. 🗑️
- For more features see our [documentation](https://docs.whitespots.io/appsec-portal/features).

**Paid version**
- [Auto validator](https://docs.whitespots.io/appsec-portal/features/auto-validator): Automated validation of security vulnerabilities to reduce triage time. ✅
- [Deduplicator](https://docs.whitespots.io/appsec-portal/features/deduplicator): Remove duplicate findings from multiple scanners with _custom rules_. 🗑️
- For more features see our [documentation](https://docs.whitespots.io/appsec-portal/features).

**Additional services**
- Quarterly audit of processes: Regular assessment of security processes for compliance and effectiveness. 📅
- Assist in pipelines setup: Expert assistance in setting up secure development pipelines. 🚀
- Application Security course for developers ([One module example](https://appsec-learning.whitespots.io)). 🎓

## Latest stable release tag

![Stable](https://gitlab.com/whitespots-public/appsec-portal/-/badges/release.svg?order_by=release_at&value_width=150)

## Unstable release tag
latest

## Documentation

For detailed documentation, please refer to our [documentation](https://docs.whitespots.io/).

## System requirements
 
* Minimum system resources: 4 GB of RAM and 2 CPU cores.
* Free disk space for installation and data storage of the portal.
* Network access for external users (users must be able to connect to the portal over the network).</e>

[//]: # (**GitLab**:)

[//]: # (* Minimum system resources: 8 GB of RAM and 4 CPU cores.  )

[//]: # (* Free disk space for installation and storage of GitLab and related data. )

[//]: # (* Network access for external users &#40;users must be able to connect to GitLab over the network&#41;.)

[//]: # ()
[//]: # (**Connectivity between system components**:)

[//]: # (* Ensure access from the GitLab server to the server with GitLab Runner. )

[//]: # (* Ensure access from the GitLab Runner server to the server with the portal.)

# Deployment process

* **Install**:
  
  * [Deploy using GitLab CI (Docker-compose installation)](/README.md#gitlab-ci-installation)
  * [Install using Helm (Install in Kubernetes environment)](/README.md#install-using-helm)
  * [Manual Installation (Docker-compose installation)](/README.md#manual-installation)


* **Update:**
  
  * [Update using GitLab CI](/README.md#update-using-gitlab-ci-)
  * [Update using Helm](/README.md#update-in-kubernetes-environment)
  * [Manual update](/README.md#manual-update)
 
    

## Installation

### Prerequisites
Before installing the AppSec Portal, make sure you have the following software installed on your machine (for Docker-compose installation type):
* [**Docker**](https://docs.docker.com/get-docker/) (version 19.03 or higher)
* [**Docker Compose**](https://docs.docker.com/compose/install/) (version 1.26 or higher)
* [**GitLab Runner**](https://docs.gitlab.com/runner/install/) (for GitLab CI installation option)
* **SSH keys**

<details><summary>Expand to see how to generate SSH keys</summary><br/>
To securely connect to the Linux server, you will need to set up SSH keys.\
If you don't have SSH keys already, you can generate them using the following command in your server terminal:</e>

```bash
ssh-keygen -t rsa -b 4096
```

After generating the SSH keys, you need to copy the public SSH key to the Linux server. Use this command to copy the public key:

```bash
ssh-copy-id <username>@<server-ip-address>
```

Replace ***username*** with your Linux server account username, and ***server-ip-address*** with the IP address of the Linux server. You will be prompted to enter your password for authentication.\
Open the file on your local machine where the private SSH key is stored. The private key is typically saved with a .pem or .ssh file extension.\
Select and copy the contents of the private key file.\
Ensure you copy the key with the correct permissions and line breaks intact.</details>


###  GitLab CI installation

#### Step 1: Fork the AppSec Portal repository.

Fork the AppsecPortal repository on GitLab. This creates a copy of the repository under your GitLab account.

#### Step 2: Set the public SSH key on the host where the portal will be deployed.
Establish a secure connection between the host and the repository by setting the public SSH key.

#### Step 3: [Configure Environment Variables](https://docs.gitlab.com/ee/ci/variables/)
The script contains:

* **mandatory** environment variables that must be specified (In the *CI/CD settings*, you need to set the following environment variables for these keys):


**SEC_PORTAL_HOST**: Specify the host where the portal will be deployed
**SEC_PORTAL_USER**: Provide the username under which the installation process will be executed on the host machine using the provided SSH key for authentication                                    
**SSH_KEY_PRIVATE**: Set the private SSH key within the forked repository. This key will be used for    authentication during the installation process
**DOMAIN**: Specify the domain where the AppSec Portal will be accessible


* **optional** environment variables. You can choose to accept the default values provided for demonstration purposes or change if necessary:

````bash
IMAGE_VERSION=latest
DB_NAME=db_name
DB_USER=db_user
DB_PASS=db_pass
DB_HOST=db_host
DB_PORT=5432
GUNICORN_WORKERS=1
GUNICORN_THREADS=5
IMPORTER_GUNICORN_WORKERS=1
IMPORTER_GUNICORN_THREADS=1
RABBITMQ_DEFAULT_USER=admin
RABBITMQ_DEFAULT_PASS=mypass
AMQP_HOST_STRING=amqp://admin:mypass@rabbitmq:5672/
DOMAIN=http://localhost
COOKIES_SECURE=False (True if you use https)
````
* The `IMAGE_VERSION` The script will autonomously determine the most recent version
* For optimal performance, it is recommended to specify the following environment values: `GUNICORN_WORKERS` = 4 and `GUNICORN_THREADS`= 16
* To configure the import worker and import threads, the following is necessary: 
<br>`IMPORTER_GUNICORN_WORKERS` determines the number of workers for processing import tasks. It is recommended to set a value that takes into account the volume and intensity of import tasks. 
<br> `IMPORTER_GUNICORN_THREADS` defines the number of threads within each import worker. This affects the parallel processing of tasks within the worker. 
* `DB_NAME`, `DB_USER`, `DB_PASS`, `DB_HOST`, `DB_PORT` variables are required for database configuration
* If the message broker is hosted on a third-party server, only the `AMQP_HOST_STRING` must be specified. 
However, if the container is raised locally, all three variables, including `RABBITMQ_DEFAULT_USER` and `RABBITMQ_DEFAULT_PASS` need to be specified
* The `COOKIES_SECURE` variable determines the cookie security flag. It should be set to True if HTTPS is used.

#### Step 4: Run pipeline
Click on install section

<br>![Image](docs/images/Pipeline.png)

The GitLab CI script provided in the forked repository will handle the installation process.
This script will raise the portal and generate a user with administrator privileges using the default login and password credentials "admin/admin".

⚠️ WARNING: after the initial installation, it is necessary to reset the password for the administrator user via the Django admin panel.


### Install in Kubernetes environment
### Install using Helm
Before using Helm, make sure that Helm is installed on your computer and that your Kubernetes cluster is configured to work with Helm

#### Step 1. Add helm package
Add the Appsec portal package to your server:
```bash
helm repo add appsecportal https://gitlab.com/api/v4/projects/37960926/packages/helm/stable
```

#### Step 2. Set environment variables
change the default environment variables to meet your requirements via --set:

* In the **deploymentSpec** :
```bash
global.image.tag=latest
```

* In the **ingresses** :
```bash
webhook.ingress.path=/api/v1/jira-helper/jira-event/your-webhook/
```
Replace *your-webhook* in path variable '/api/v1/jira-helper/jira-event/your-webhook/' with the unique identifier (token) associated with the specific webhook event, for example, e2b7e8be-1c77-4969-9105-58e91bd311cc.


* In the **configMap** :
```bash
  configs.configMap.cookies_secure=true
  configs.configMap.debug=true
  postgresql.auth.database=appsec
  postgresql.auth.username=appsec
  postgresql.containerPorts.postgresql=5432
  configs.configMap.database.host=your_db_host
  postgresql.enabled=false (for external db)
  rabbitmq.auth.username=admin           
  rabbitmq.containerPorts.amqp=5672
  rabbitmq.enabled=false (for external rabbitmq) 
  rabbitmq.nameOverride=rabbitmq_name (for external rabbitmq) 
```
  
* In the **secrets** section:
```bash
  postgresql.auth.password=appsec
  configs.secret.jwt_private_key=your_key
  configs.secret.jwt_public_key=your_key
  configs.secret.secret_key=your_key
  rabbitmq.auth.password=pass
```
* `release`: specify a particular release identifier, e.g. release_v23.11.1
* `configs.configMap.cookies_secure`: variable determines the cookie security flag. It should be set to `true` if HTTPS is used. 
* `postgresql.auth.database`, `postgresql.auth.username`, `configs.configMap.database.host`, `postgresql.containerPorts.postgresql` and `DB_PASS` variables are required for database configuration.    
* For message broker `rabbitmq.auth.username`, `rabbitmq.auth.password` and `rabbitmq.containerPorts.amqp` need to be specified
* The `configs.secret.jwt_private_key` and `configs.secret.jwt_public_key` variables are RSA key pair used to sign JWT keys
* `configs.secret.secret_key`: variable is used to generate hashes in Django

#### Step 3. Helm install with all resources inside cluster
In the example we use pre-installed nginx ingress controller and postgres, rabbitmq from chart:
```bash
helm install portal appsecportal/appsecportal --set postgresql.enabled=true 
    --set ingress.enabled=true --set ingress.annotations."nginx\.ingress\.kubernetes\.io\/scheme"=internet-facing
    --set ingress.annotations."nginx\.ingress\.kubernetes\.io\/target\-type"=ip --set ingress.ingressClassName=nginx 
    --set ingress.host=your_own_host -n <namespace>
```

#### Step 4. Create a superuser account

To create an administrator account, execute the following command:

```bash
kubectl get pods -n <namespace>
kubectl exec -it -n <namespace> <portal_pod> -c appsecportal-backend -- /bin/sh 
python3 manage.py createsuperuser --username admin
```

### Manual Installation

#### Step 1: Clone the repository

Clone the AppSec Portal repository to your server:

```bash
git clone https://gitlab.com/whitespots-public/appsec-portal.git appsec-portal
```

#### Step 2 Navigate to the root directory

Navigate to the root directory of the AppSec Portal project by executing the following command:

```bash
cd appsec-portal
```

#### Step 3: Set environment variables

In the root directory of the AppSec Portal project, execute the following command:

```bash
./set_vars.sh
```

The script will prompt you to provide values for the following environment variables including optional ones. You can also choose to accept the default values provided for demonstration purposes:

```bash
IMAGE_VERSION=latest
DB_NAME=db_name
DB_USER=db_user
DB_PASS=db_pass
DB_HOST=db_host
DB_PORT=5432
GUNICORN_WORKERS=1
GUNICORN_THREADS=5
IMPORTER_GUNICORN_WORKERS=1
IMPORTER_GUNICORN_THREADS=1
RABBITMQ_DEFAULT_USER=admin
RABBITMQ_DEFAULT_PASS=mypass
AMQP_HOST_STRING=amqp://admin:mypass@rabbitmq:5672/
DOMAIN=http://localhost
COOKIES_SECURE=False (True if you use https)
```

- The `IMAGE_VERSION` variable must be specified, as it is required for `docker-compose.yml`.
- For optimal performance, it is recommended to specify the following environment values: `GUNICORN_WORKERS` = **4** and `GUNICORN_THREADS`= **16**
<br> To configure the import worker and import threads, the following is necessary: 
<br>`IMPORTER_GUNICORN_WORKERS` determines the number of workers for processing import tasks. It is recommended to set a value that takes into account the volume and intensity of import tasks. 
<br>`IMPORTER_GUNICORN_THREADS` defines the number of threads within each import worker. This affects the parallel processing of tasks within the worker.
<br>-`DB_NAME`, `DB_USER`, `DB_PASS`, `DB_HOST`, `DB_PORT` variables are required for database configuration.

- If the message broker is hosted on a third-party server, only the `AMQP_HOST_STRING` must be specified. However, if the container is raised locally, all three variables, including `RABBITMQ_DEFAULT_USER` and `RABBITMQ_DEFAULT_PASS` need to be specified.
- The `DOMAIN` Specify the domain where the AppSec Portal will be accessible.
- The `COOKIES_SECURE` variable determines the cookie security flag. It should be set to `True` if HTTPS is used.

Or just add only essential variables needed for the AppSec Portal to function properly:

```bash
IMAGE_VERSION=latest
DOMAIN=http://localhost
```

The `set_vars.sh` script will create the `.env` file with the configured environment variables and generate a pair of JWT keys and `SECRET_KEY`:

```bash
JWT_PRIVATE_KEY=-----BEGIN RSA PRIVATE KEY-----\nMIICX...
JWT_PUBLIC_KEY=-----BEGIN PUBLIC KEY-----\nMIGf...
SECRET_KEY=django_secret_key
```

> **⚠️ WARNING:** The values of `JWT_PRIVATE_KEY` and `JWT_PUBLIC_KEY` provided in the example above are for demonstration purposes only. If you're using this application in production, you should run set_vars.sh script to specify new pair of keys in the environment. This applies to any other passwords as well.

#### Step 4. Start the AppSec Portal

To start the AppSec Portal, run the following command:

```bash
sh run.sh
```

#### Step 5. Create a superuser account

To create an administrator account, execute the following command:

```bash
docker-compose exec back python3 manage.py createsuperuser --username admin
```

#### Step 6. Configure the app

**Create users using Django admin panel**

In order to access admin settings, follow the `<your-domain>.com/admin` URL and sign in
using the superuser credentials, then select "**Users**" in the left panel. You can add users from there.

**Complete JIRA integration**

AppSec Portal provides _bi-directional_ integration with Jira.

For the best experience, [JIRA WebHook service integration](https://docs.whitespots.io/appsec-portal/integrations/jira/webhook-integration) is recommended. You should [create a webhook](https://docs.whitespots.io/appsec-portal/integrations/jira/creating-a-webhook-in-jira) with the following configuration:

- Enter the [URL from AppSec Portal](https://docs.whitespots.io/appsec-portal/integrations/jira/creating-a-webhook-in-jira#understanding-the-webhook-url) in the "**URL**" field.
- Select the "**Issue related events**" that will trigger the webhook: **updated** and **deleted** checkboxes in "**Issue**" column.
- Select the scope: **all issues**.
- Change the "**Exclude body**" option to "**No**".

**Integration with DefectDojo**

With this integration, you can quickly and easily import your vulnerabilities from DefectDojo and manage them alongside other vulnerability data collected from multiple sources.

AppSec Portal supports integration with **DefectDojo**, but _not all of it's functionality is bi-directional_. See the [documentation](https://docs.whitespots.io/appsec-portal/integrations/defectdojo) for more information.


## How to update AppSec Portal

To update the AppSec Portal to the latest version, follow these steps:

### Update using GitLab CI 

1. [Update](https://docs.gitlab.com/ee/user/project/repository/mirror/index.html) your forked repository

2. Run pipeline

3. Click on **update** section<br/>

![Image](docs/images/Pipeline.png)


### Update using helm:
1. To update, run the following command:
```bash
helm upgrade appsecportal  <path-to-helm-directory>
```
replace *<path-to-helm-directory>* with the path to the directory that contains the Helm Chart for your application.

### Manual update

1. Stop the application:

```bash
docker-compose down -v
```
This will stop all services and remove the associated volumes, which will clean up the environment.
2. Pull the latest changes from the repository:

```bash
git pull
```

This ensures that you have the most up-to-date codebase to work with.

3. Restart the application:

```bash
docker-compose up -d
```
